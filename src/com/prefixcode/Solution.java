package com.prefixcode;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

public class Solution {
	static String convertIntegerToSentence(String numbers, Map<String, Integer> map) {
		String[] numbersArray = numbers.split(" ");
		String result = "";
		for (int i = 0; i < numbersArray.length; i++) {
			for (Map.Entry<String, Integer> entry : map.entrySet()) {
				if (entry.getValue() == Integer.parseInt(numbersArray[i])) {
					Character c = convertIntegerToCharacter(entry.getValue());
					result += c + "";
				}
			}
		}
		return result;
	}

	static Character convertIntegerToCharacter(int value) {
		char c = (char) value;
		return new Character(c);
	}

	static StringBuilder getCode(String INPUT, Map<String, Integer> map) {
		StringBuilder code = new StringBuilder();
		while (!"".equals(INPUT)) {
			char firstChar = INPUT.charAt(0);
			LinkedHashMap<String, Integer> tmpMap = new LinkedHashMap<>();
			for (Map.Entry<String, Integer> entry : map.entrySet()) {
				if (firstChar == entry.getKey().charAt(0)) {
					tmpMap.put(entry.getKey(), entry.getKey().length());
				}
			}
			TreeSet<Integer> lengths = new TreeSet<>();
			LinkedHashMap<String, Integer> tmpSortedMap = sortMapByValuesDesc(tmpMap);
			int maxLength = 0;
			for (Map.Entry<String, Integer> entry : tmpSortedMap.entrySet()) {
				maxLength = 0;
				String keyTmpSortedMap = entry.getKey();
				for (int j = 0; j < keyTmpSortedMap.length(); j++) {
					if (INPUT.charAt(j) == entry.getKey().charAt(j)) {
						maxLength++;
						lengths.add(maxLength);
					} else {
						break;
					}
				}
			}
			String foundedValue = INPUT.substring(0, lengths.last());
			int inputLength = INPUT.length();
			INPUT = INPUT.substring(lengths.last(), inputLength);
			code.append(map.get(foundedValue));
			code.append(" ");
		}
		return code;
	}

	static LinkedHashMap<String, Integer> sortMapByValuesDesc(LinkedHashMap<String, Integer> map) {
		List<Map.Entry<String, Integer>> list = new LinkedList<>(map.entrySet());

		Collections.sort(list, (o1, o2) -> o1.getValue().compareTo(o2.getValue()) > 0 ? -1 : 1);

		LinkedHashMap<String, Integer> result = new LinkedHashMap<>();

		for (Map.Entry<String, Integer> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}

		return result;
	}
}
